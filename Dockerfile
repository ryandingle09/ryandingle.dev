from ubuntu:latest

RUN apt-get update && apt-get install -y software-properties-common && apt-get update
RUN add-apt-repository universe -y && add-apt-repository ppa:certbot/certbot -y
RUN apt-get install -y nginx redis-server certbot python-certbot-nginx curl libmysqlclient-dev

RUN echo Updating existing packages, installing and upgrading python and pip.
RUN apt-get update -y
RUN apt-get install -y python3 python3-pip python3-dev build-essential
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools

# install frontend framework of your choice
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g create-react-app

ADD ./backend/requirements.txt /app/
ADD ./frontend/package.json /app/
WORKDIR /app

# install backend dependecies
RUN echo Installing Python packages listed in requirements.txt
RUN pip3 install -r ./requirements.txt
RUN echo Starting python and starting the Flask service...

# install frontend dependecies
WORKDIR /app/frontend
RUN npm install
RUN ls /app
RUN mv /app/node_modules /app/frontend
RUN ls /app/frontend
WORKDIR /app

COPY . /app

COPY ./nginx_conf/ryandingle.dev.conf /etc/nginx/sites-available/
COPY ./nginx_conf/www.ryandingle.dev.conf /etc/nginx/sites-available/
COPY ./nginx_conf/api.ryandingle.dev.conf /etc/nginx/sites-available/

RUN ln -s /etc/nginx/sites-available/ryandingle.dev.conf /etc/nginx/sites-enabled/
RUN ln -s /etc/nginx/sites-available/www.ryandingle.dev.conf /etc/nginx/sites-enabled/
RUN ln -s /etc/nginx/sites-available/api.ryandingle.dev.conf /etc/nginx/sites-enabled/

EXPOSE 8080 80 3000 8000

CMD ["bash", "start.sh"]