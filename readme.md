# build the image
sudo docker build -t ryandingle09/ryandingle .

# run the image in the container
sudo docker run -it --rm --name ryandingle -d -p 8080:8080 -p 80:80 -p 3000:3000 ryandingle09/ryandingle

# stop containenr
sudo docker container stop ryandingle 